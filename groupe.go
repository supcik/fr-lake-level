// Copyright 2018 Jacques Supcik
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// This file implements the method for scraping the site of "Group-e"
// and extracting information about the level of some lakes in
// Canton of Fribourg

package lakelevel

import (
	"io"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/pkg/errors"
)

// Lake is the structure for storing lake information.
type Lake struct {
	Name     string                `json:"name"`
	MaxLevel float64               `json:"max_level"`
	Levels   map[time.Time]float64 `json:"levels"`
}

// Lakes is the list of all fetched lakes.
type Lakes map[string]Lake

func msm(t string) (float64, error) {
	re := regexp.MustCompile(`(\d+\.\d+).*msm`)
	n := re.FindStringSubmatch(t)
	if n != nil {
		nf, err := strconv.ParseFloat(n[1], 64)
		if err == nil {
			return nf, nil
		}
	}
	return 0, errors.New("Unable to parse msm")
}

func scrape(r io.Reader) (Lakes, error) {
	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		return nil, err
	}
	result := make(Lakes)
	table := doc.Find("table").First()
	header := table.Find("thead tr th")
	date1, err := time.Parse("2.1.2006", strings.TrimSpace(header.Eq(2).Text()))
	if err != nil {
		return nil, err
	}
	date2, err := time.Parse("2.1.2006", strings.TrimSpace(header.Eq(3).Text()))
	if err != nil {
		return nil, err
	}
	body := table.Find("tbody tr")
	body.Each(func(i int, selection *goquery.Selection) {
		name := strings.TrimSpace(selection.Find("td").Eq(0).Text())
		maxLevel, _ := msm(strings.TrimSpace(selection.Find("td").Eq(1).Text()))
		l1, _ := msm(strings.TrimSpace(selection.Find("td").Eq(2).Text()))
		l2, _ := msm(strings.TrimSpace(selection.Find("td").Eq(3).Text()))
		lake := Lake{
			Name:     name,
			MaxLevel: maxLevel,
			Levels: map[time.Time]float64{
				date1: l1,
				date2: l2,
			},
		}
		result[name] = lake
	})
	return result, nil
}

// LatestLevel returns the latest level of the lake.
func (l Lake) LatestLevel() float64 {
	var result float64 = -1
	var latest time.Time
	for k, v := range l.Levels {
		if result < 0 || k.After(latest) {
			latest = k
			result = v
		}
	}
	return result
}
