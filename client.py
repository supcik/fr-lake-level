#!/usr/bin/env python2

from OmegaExpansion import oledExp
import urllib2

val = urllib2.urlopen("https://niveaux-lacs-fribourg.appspot.com/gruyere").read()

oledExp.driverInit()
status  = oledExp.setCursor(0, 0)
status  |= oledExp.write("Niveau du lac: " + val)
