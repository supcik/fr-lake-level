// Copyright 2018 Jacques Supcik
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// This program fetches the level of some lakes in the canton of Fribourg
// and makes them available for simple web sites of for IoT.

package lakelevel

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"github.com/gorilla/mux"
	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/urlfetch"
)

const (
	dweetBase = "https://dweet.io/dweet/for/niveaux-des-lacs-fribourg-ch"
)

var id2name = map[string]string{
	"gruyere":     "La Gruyère",
	"schiffenen":  "Schiffenen",
	"lessoc":      "Lessoc",
	"montsalvens": "Montsalvens",
	"moron":       "Moron",
	"brenets":     "Les Brenets",
	"rossiniere":  "Rossinière",
	"g":           "La Gruyère",
	"s":           "Schiffenen",
	"l":           "Lessoc",
	"m":           "Montsalvens",
	"o":           "Moron",
	"b":           "Les Brenets",
	"r":           "Rossinière",
}

var dweetNames = [...]string{"gruyere", "schiffenen", "lessoc", "montsalvens", "moron", "rossiniere"}

func fetchData(ctx context.Context) (Lakes, error) {
	client := urlfetch.Client(ctx)
	resp, err := client.Get("https://www.groupe-e.ch/fr/univers-groupe-e/niveau-lacs")
	if err != nil {
		return nil, err
	}
	return scrape(resp.Body)
}

func updateDweet(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	lakes, err := fetchData(ctx)
	if err != nil {
		log.Errorf(ctx, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	u, err := url.Parse(dweetBase)
	if err != nil {
		log.Errorf(ctx, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	query := u.Query()
	for _, id := range dweetNames {
		query.Add(fmt.Sprintf("%s-cur", id), fmt.Sprintf("%.2f", lakes[id2name[id]].LatestLevel()))
		query.Add(fmt.Sprintf("%s-max", id), fmt.Sprintf("%.2f", lakes[id2name[id]].MaxLevel))
		query.Add(fmt.Sprintf("%s-dif", id), fmt.Sprintf("%.2f", lakes[id2name[id]].LatestLevel()-lakes[id2name[id]].MaxLevel))
	}
	u.RawQuery = query.Encode()

	client := urlfetch.Client(ctx)
	resp, err := client.Get(u.String())
	if err != nil {
		log.Errorf(ctx, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, resp)
}

func raw(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	lakes, err := fetchData(ctx)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	j, err := json.Marshal(lakes)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, string(j))
}

func currentLevel(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	lakeID := vars["lake"]
	id, ok := id2name[lakeID]
	if !ok {
		http.Error(w, fmt.Sprintf("Lake '%s' not found", lakeID), http.StatusInternalServerError)
		return
	}
	ctx := appengine.NewContext(r)
	lakes, err := fetchData(ctx)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "text/plain")
	mod, ok := vars["mod"]
	if ok && (mod == "max" || mod == "m") {
		fmt.Fprintf(w, "%.2f", lakes[id].MaxLevel)
	} else if ok && (mod == "dif" || mod == "d") {
		fmt.Fprintf(w, "%.2f", lakes[id].LatestLevel()-lakes[id].MaxLevel)
	} else {
		fmt.Fprintf(w, "%.2f", lakes[id].LatestLevel())
	}

}

func init() {
	r := mux.NewRouter()
	r.HandleFunc("/raw", raw)
	r.HandleFunc("/update", updateDweet)
	r.HandleFunc("/level/{lake}", currentLevel)
	r.HandleFunc("/level/{lake}/{mod:(?:cur|max|dif)}", currentLevel)
	r.HandleFunc("/{lake}", currentLevel)
	r.HandleFunc("/{lake}/{mod:(?:c|m|d)}", currentLevel)
	http.Handle("/", r)
}
